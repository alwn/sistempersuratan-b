import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  
  {
    path: 'dashboard-kepsek',
    loadChildren: () => import('./user-kepsek/dashboard-kepsek/dashboard-kepsek.module').then( m => m.DashboardKepsekPageModule)
  },
  {
    path: 'suratmasuk-kepsek',
    loadChildren: () => import('./user-kepsek/suratmasuk-kepsek/suratmasuk-kepsek.module').then( m => m.SuratmasukKepsekPageModule)
  },
  {
    path: 'profile-kepsek',
    loadChildren: () => import('./user-kepsek/profile-kepsek/profile-kepsek.module').then( m => m.ProfileKepsekPageModule)
  },
  
  {
    path: 'dashboard-tu',
    loadChildren: () => import('./user-tatausaha/dashboard-tu/dashboard-tu.module').then( m => m.DashboardTuPageModule)
  },
  {
    path: 'profile-tu',
    loadChildren: () => import('./user-tatausaha/profile-tu/profile-tu.module').then( m => m.ProfileTuPageModule)
  },
  {
    path: 'suratmasuk-tu',
    loadChildren: () => import('./user-tatausaha/suratmasuk-tu/suratmasuk-tu.module').then( m => m.SuratmasukTuPageModule)
  },
  {
    path: 'suratterkirim-tu',
    loadChildren: () => import('./user-tatausaha/suratterkirim-tu/suratterkirim-tu.module').then( m => m.SuratterkirimTuPageModule)
  },
  {
    path: 'tambahsurat-tu',
    loadChildren: () => import('./user-tatausaha/tambahsurat-tu/tambahsurat-tu.module').then( m => m.TambahsuratTuPageModule)
  },
  {
    path: 'terusan-tu',
    loadChildren: () => import('./user-tatausaha/terusan-tu/terusan-tu.module').then( m => m.TerusanTuPageModule)
  },
  {
    path: 'tugas-tu',
    loadChildren: () => import('./user-tatausaha/tugas-tu/tugas-tu.module').then( m => m.TugasTuPageModule)
  },
  {
    path: 'dashboard-guru',
    loadChildren: () => import('./user-guru/dashboard-guru/dashboard-guru.module').then( m => m.DashboardGuruPageModule)
  },
  {
    path: 'profile-guru',
    loadChildren: () => import('./user-guru/profile-guru/profile-guru.module').then( m => m.ProfileGuruPageModule)
  },
  {
    path: 'suratmasuk-guru',
    loadChildren: () => import('./user-guru/suratmasuk-guru/suratmasuk-guru.module').then( m => m.SuratmasukGuruPageModule)
  },
  {
    path: 'suratterkirim-guru',
    loadChildren: () => import('./user-guru/suratterkirim-guru/suratterkirim-guru.module').then( m => m.SuratterkirimGuruPageModule)
  },
  {
    path: 'tambahsurat-guru',
    loadChildren: () => import('./user-guru/tambahsurat-guru/tambahsurat-guru.module').then( m => m.TambahsuratGuruPageModule)
  },
  {
    path: 'tugas-guru',
    loadChildren: () => import('./user-guru/tugas-guru/tugas-guru.module').then( m => m.TugasGuruPageModule)
  },
  {
    path: 'detailsurat-tu/:id',
    loadChildren: () => import('./user-tatausaha/detailsurat-tu/detailsurat-tu.module').then( m => m.DetailsuratTuPageModule)
  },
 
  {
    path: 'suratterkirim-kepsek',
    loadChildren: () => import('./user-kepsek/suratterkirim-kepsek/suratterkirim-kepsek.module').then( m => m.SuratterkirimKepsekPageModule)
  },
  {
    path: 'tugas-kepsek',
    loadChildren: () => import('./user-kepsek/tugas-kepsek/tugas-kepsek.module').then( m => m.TugasKepsekPageModule)
  },
  {
    path: 'tindaklanjut-kepsek',
    loadChildren: () => import('./user-kepsek/tindaklanjut-kepsek/tindaklanjut-kepsek.module').then( m => m.TindaklanjutKepsekPageModule)
  },
  {
    path: 'detailsurat-kepsek/:id',
    loadChildren: () => import('./user-kepsek/detailsurat-kepsek/detailsurat-kepsek.module').then( m => m.DetailsuratKepsekPageModule)
  },
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
