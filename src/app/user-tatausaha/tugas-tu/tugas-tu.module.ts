import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TugasTuPageRoutingModule } from './tugas-tu-routing.module';

import { TugasTuPage } from './tugas-tu.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TugasTuPageRoutingModule
  ],
  declarations: [TugasTuPage]
})
export class TugasTuPageModule {}
