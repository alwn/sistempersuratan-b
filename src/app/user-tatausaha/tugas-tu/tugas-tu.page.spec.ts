import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TugasTuPage } from './tugas-tu.page';

describe('TugasTuPage', () => {
  let component: TugasTuPage;
  let fixture: ComponentFixture<TugasTuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TugasTuPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TugasTuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
