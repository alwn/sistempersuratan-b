import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TugasTuPage } from './tugas-tu.page';

const routes: Routes = [
  {
    path: '',
    component: TugasTuPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TugasTuPageRoutingModule {}
