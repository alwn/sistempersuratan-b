import { Component, OnInit } from '@angular/core';
import { AngularFirestore  } from "@angular/fire/firestore";

@Component({
  selector: 'app-tambahsurat-tu',
  templateUrl: './tambahsurat-tu.page.html',
  styleUrls: ['./tambahsurat-tu.page.scss'],
})
export class TambahsuratTuPage implements OnInit {

  constructor(
    public db: AngularFirestore
  ) { }

  ngOnInit() {
  }

  data: any = {};
  tambahdata(){
    console.log(this.data)
    this.db.collection('terkirim-tu').doc(this.data.no_surat).set(this.data).then(data => {
      alert('surat berhasil ditambahkan')
    })
  }
}
