import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TambahsuratTuPageRoutingModule } from './tambahsurat-tu-routing.module';

import { TambahsuratTuPage } from './tambahsurat-tu.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TambahsuratTuPageRoutingModule
  ],
  declarations: [TambahsuratTuPage]
})
export class TambahsuratTuPageModule {}
