import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TambahsuratTuPage } from './tambahsurat-tu.page';

const routes: Routes = [
  {
    path: '',
    component: TambahsuratTuPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TambahsuratTuPageRoutingModule {}
