import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TambahsuratTuPage } from './tambahsurat-tu.page';

describe('TambahsuratTuPage', () => {
  let component: TambahsuratTuPage;
  let fixture: ComponentFixture<TambahsuratTuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TambahsuratTuPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TambahsuratTuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
