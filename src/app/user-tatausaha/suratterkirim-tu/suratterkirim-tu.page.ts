import { Component, OnInit } from '@angular/core';
import { AngularFirestore  } from "@angular/fire/firestore";

@Component({
  selector: 'app-suratterkirim-tu',
  templateUrl: './suratterkirim-tu.page.html',
  styleUrls: ['./suratterkirim-tu.page.scss'],
})
export class SuratterkirimTuPage implements OnInit {

  constructor(
    public db: AngularFirestore
  ) { }

  ngOnInit() {
    this.ambildata();

  }

  surat_terkirim: any;

  ambildata(){
    this.db.collection('terkirim-tu').valueChanges().subscribe(data =>{
      this.surat_terkirim=data;
      console.log(this.surat_terkirim)
    } )
  }

}