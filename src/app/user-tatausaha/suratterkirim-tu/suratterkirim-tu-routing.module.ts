import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SuratterkirimTuPage } from './suratterkirim-tu.page';

const routes: Routes = [
  {
    path: '',
    component: SuratterkirimTuPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SuratterkirimTuPageRoutingModule {}
