import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SuratterkirimTuPage } from './suratterkirim-tu.page';

describe('SuratterkirimTuPage', () => {
  let component: SuratterkirimTuPage;
  let fixture: ComponentFixture<SuratterkirimTuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuratterkirimTuPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SuratterkirimTuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
