import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SuratterkirimTuPageRoutingModule } from './suratterkirim-tu-routing.module';

import { SuratterkirimTuPage } from './suratterkirim-tu.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SuratterkirimTuPageRoutingModule
  ],
  declarations: [SuratterkirimTuPage]
})
export class SuratterkirimTuPageModule {}
