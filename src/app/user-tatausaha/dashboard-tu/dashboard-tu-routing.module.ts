import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardTuPage } from './dashboard-tu.page';

const routes: Routes = [
  {
    path: '',
    component: DashboardTuPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardTuPageRoutingModule {}
