import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DashboardTuPageRoutingModule } from './dashboard-tu-routing.module';

import { DashboardTuPage } from './dashboard-tu.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DashboardTuPageRoutingModule
  ],
  declarations: [DashboardTuPage]
})
export class DashboardTuPageModule {}
