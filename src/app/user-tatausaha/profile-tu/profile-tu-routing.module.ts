import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfileTuPage } from './profile-tu.page';

const routes: Routes = [
  {
    path: '',
    component: ProfileTuPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileTuPageRoutingModule {}
