import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfileTuPageRoutingModule } from './profile-tu-routing.module';

import { ProfileTuPage } from './profile-tu.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfileTuPageRoutingModule
  ],
  declarations: [ProfileTuPage]
})
export class ProfileTuPageModule {}
