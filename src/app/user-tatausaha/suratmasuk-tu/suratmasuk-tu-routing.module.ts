import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SuratmasukTuPage } from './suratmasuk-tu.page';

const routes: Routes = [
  {
    path: '',
    component: SuratmasukTuPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SuratmasukTuPageRoutingModule {}
