import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SuratmasukTuPageRoutingModule } from './suratmasuk-tu-routing.module';

import { SuratmasukTuPage } from './suratmasuk-tu.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SuratmasukTuPageRoutingModule
  ],
  declarations: [SuratmasukTuPage]
})
export class SuratmasukTuPageModule {}
