import { Component, OnInit } from '@angular/core';
import { AngularFirestore  } from "@angular/fire/firestore";
import { Router } from '@angular/router';

@Component({
  selector: 'app-suratmasuk-tu',
  templateUrl: './suratmasuk-tu.page.html',
  styleUrls: ['./suratmasuk-tu.page.scss'],
})
export class SuratmasukTuPage implements OnInit {

  constructor(
    public db: AngularFirestore,
    private router: Router
    ) { }

  ngOnInit() {
    this.ambildata();
  }
  uid:any=[];
  data: any= {};

  surat_masuk: any;

  detail(data) {
    console.log(data)
    this.router.navigate(['/detailsurat-tu/',data.id]);
  }

  ambildata(){
    this.db.collection('disposisi').valueChanges({idField: 'id'}).subscribe(data =>{
      this.surat_masuk=data;
      console.log(this.surat_masuk)
    } )
  }

}
