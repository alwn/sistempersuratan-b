import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SuratmasukTuPage } from './suratmasuk-tu.page';

describe('SuratmasukTuPage', () => {
  let component: SuratmasukTuPage;
  let fixture: ComponentFixture<SuratmasukTuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuratmasukTuPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SuratmasukTuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
