import { Component, OnInit } from '@angular/core';
import { AngularFirestore  } from "@angular/fire/firestore";

@Component({
  selector: 'app-terusan-tu',
  templateUrl: './terusan-tu.page.html',
  styleUrls: ['./terusan-tu.page.scss'],
})
export class TerusanTuPage implements OnInit {

  constructor(
    public db: AngularFirestore
  ) { }

  ngOnInit() {
  }

  data: any = {};
  tambahdata(){
    console.log(this.data)
    this.db.collection('terkirim-tu').doc(this.data.no_surat).set(this.data).then(data => {
      alert('surat berhasil ditambahkan')
    })
  }
}
