import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TerusanTuPage } from './terusan-tu.page';

const routes: Routes = [
  {
    path: '',
    component: TerusanTuPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TerusanTuPageRoutingModule {}
