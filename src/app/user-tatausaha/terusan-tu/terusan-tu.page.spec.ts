import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TerusanTuPage } from './terusan-tu.page';

describe('TerusanTuPage', () => {
  let component: TerusanTuPage;
  let fixture: ComponentFixture<TerusanTuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TerusanTuPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TerusanTuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
