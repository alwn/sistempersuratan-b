import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TerusanTuPageRoutingModule } from './terusan-tu-routing.module';

import { TerusanTuPage } from './terusan-tu.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TerusanTuPageRoutingModule
  ],
  declarations: [TerusanTuPage]
})
export class TerusanTuPageModule {}
