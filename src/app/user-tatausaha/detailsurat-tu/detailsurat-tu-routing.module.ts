import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailsuratTuPage } from './detailsurat-tu.page';

const routes: Routes = [
  {
    path: '',
    component: DetailsuratTuPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailsuratTuPageRoutingModule {}
