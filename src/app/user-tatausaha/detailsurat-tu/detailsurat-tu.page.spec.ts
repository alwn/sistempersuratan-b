import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailsuratTuPage } from './detailsurat-tu.page';

describe('DetailsuratTuPage', () => {
  let component: DetailsuratTuPage;
  let fixture: ComponentFixture<DetailsuratTuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsuratTuPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailsuratTuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
