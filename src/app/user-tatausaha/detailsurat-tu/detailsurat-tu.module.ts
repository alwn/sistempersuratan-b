import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailsuratTuPageRoutingModule } from './detailsurat-tu-routing.module';

import { DetailsuratTuPage } from './detailsurat-tu.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailsuratTuPageRoutingModule
  ],
  declarations: [DetailsuratTuPage]
})
export class DetailsuratTuPageModule {}
