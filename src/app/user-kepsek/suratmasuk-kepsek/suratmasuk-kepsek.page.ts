import { Component, OnInit } from '@angular/core';
import { AngularFirestore  } from "@angular/fire/firestore";
import { Router } from '@angular/router';

@Component({
  selector: 'app-suratmasuk-kepsek',
  templateUrl: './suratmasuk-kepsek.page.html',
  styleUrls: ['./suratmasuk-kepsek.page.scss'],
})
export class SuratmasukKepsekPage implements OnInit {

  constructor(
    public db: AngularFirestore,
    private router: Router
  ) { }

  ngOnInit() {
    this.ambildata();

  }
  uid:any=[];
  data: any= {};

  surat_terkirim: any;

  detail(data) {
    console.log(data)
    this.router.navigate(['/detailsurat-kepsek/',data.id]);
  }

  ambildata(){
    this.db.collection('terkirim-tu').valueChanges({idField: 'id'}).subscribe(data =>{
      this.surat_terkirim=data;
      console.log(this.surat_terkirim)
    } )
    
  }

}

