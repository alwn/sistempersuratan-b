import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SuratmasukKepsekPage } from './suratmasuk-kepsek.page';

describe('SuratmasukKepsekPage', () => {
  let component: SuratmasukKepsekPage;
  let fixture: ComponentFixture<SuratmasukKepsekPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuratmasukKepsekPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SuratmasukKepsekPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
