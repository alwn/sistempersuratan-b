import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SuratmasukKepsekPageRoutingModule } from './suratmasuk-kepsek-routing.module';

import { SuratmasukKepsekPage } from './suratmasuk-kepsek.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SuratmasukKepsekPageRoutingModule
  ],
  declarations: [SuratmasukKepsekPage]
})
export class SuratmasukKepsekPageModule {}
