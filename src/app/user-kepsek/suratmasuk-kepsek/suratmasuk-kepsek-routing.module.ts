import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SuratmasukKepsekPage } from './suratmasuk-kepsek.page';

const routes: Routes = [
  {
    path: '',
    component: SuratmasukKepsekPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SuratmasukKepsekPageRoutingModule {}
