import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailsuratKepsekPageRoutingModule } from './detailsurat-kepsek-routing.module';

import { DetailsuratKepsekPage } from './detailsurat-kepsek.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailsuratKepsekPageRoutingModule
  ],
  declarations: [DetailsuratKepsekPage]
})
export class DetailsuratKepsekPageModule {}
