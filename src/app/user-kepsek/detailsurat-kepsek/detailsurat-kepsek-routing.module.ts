import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailsuratKepsekPage } from './detailsurat-kepsek.page';

const routes: Routes = [
  {
    path: '',
    component: DetailsuratKepsekPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailsuratKepsekPageRoutingModule {}
