import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailsuratKepsekPage } from './detailsurat-kepsek.page';

describe('DetailsuratKepsekPage', () => {
  let component: DetailsuratKepsekPage;
  let fixture: ComponentFixture<DetailsuratKepsekPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsuratKepsekPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailsuratKepsekPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
