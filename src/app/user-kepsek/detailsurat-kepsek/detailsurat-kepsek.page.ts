import { Component, OnInit } from '@angular/core';
import { AngularFirestore  } from "@angular/fire/firestore";
import { NavController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-detailsurat-kepsek',
  templateUrl: './detailsurat-kepsek.page.html',
  styleUrls: ['./detailsurat-kepsek.page.scss'],
})
export class DetailsuratKepsekPage implements OnInit {

  id_surat:any;
  constructor(
    public db: AngularFirestore,
    private navCtrl: NavController,
    public router: Router,
    private activeRoute: ActivatedRoute
  ) {
    this.id_surat = activeRoute.snapshot.paramMap.get('id');
    console.log(this.id_surat)
    if(this.id_surat != undefined) {
      this.detail();
    }
  }

  ngOnInit() {
  }

  dataSurat: any;
  date:any;
  detail(){
    this.db.collection('disposisi').doc(this.id_surat).valueChanges().subscribe(res =>{
      this.dataSurat = res;
      console.log(this.dataSurat);
      if(this.dataSurat.tanggal != undefined) {
        this.date = new Date(this.dataSurat.tanggal);
      }
    });
  }
  
  chooseFile(event) {
    console.log(event);
    //isi fungsi untuk upload file
    
  }


}
