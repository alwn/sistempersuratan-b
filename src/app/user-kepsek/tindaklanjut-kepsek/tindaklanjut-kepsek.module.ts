import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TindaklanjutKepsekPageRoutingModule } from './tindaklanjut-kepsek-routing.module';

import { TindaklanjutKepsekPage } from './tindaklanjut-kepsek.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TindaklanjutKepsekPageRoutingModule
  ],
  declarations: [TindaklanjutKepsekPage]
})
export class TindaklanjutKepsekPageModule {}
