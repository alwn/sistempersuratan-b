import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TindaklanjutKepsekPage } from './tindaklanjut-kepsek.page';

const routes: Routes = [
  {
    path: '',
    component: TindaklanjutKepsekPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TindaklanjutKepsekPageRoutingModule {}
