import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TindaklanjutKepsekPage } from './tindaklanjut-kepsek.page';

describe('TindaklanjutKepsekPage', () => {
  let component: TindaklanjutKepsekPage;
  let fixture: ComponentFixture<TindaklanjutKepsekPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TindaklanjutKepsekPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TindaklanjutKepsekPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
