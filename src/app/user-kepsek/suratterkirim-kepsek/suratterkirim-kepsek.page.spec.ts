import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SuratterkirimKepsekPage } from './suratterkirim-kepsek.page';

describe('SuratterkirimKepsekPage', () => {
  let component: SuratterkirimKepsekPage;
  let fixture: ComponentFixture<SuratterkirimKepsekPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuratterkirimKepsekPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SuratterkirimKepsekPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
