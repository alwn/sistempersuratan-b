import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SuratterkirimKepsekPageRoutingModule } from './suratterkirim-kepsek-routing.module';

import { SuratterkirimKepsekPage } from './suratterkirim-kepsek.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SuratterkirimKepsekPageRoutingModule
  ],
  declarations: [SuratterkirimKepsekPage]
})
export class SuratterkirimKepsekPageModule {}
