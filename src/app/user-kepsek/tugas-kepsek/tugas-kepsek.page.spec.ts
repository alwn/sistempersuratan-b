import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TugasKepsekPage } from './tugas-kepsek.page';

describe('TugasKepsekPage', () => {
  let component: TugasKepsekPage;
  let fixture: ComponentFixture<TugasKepsekPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TugasKepsekPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TugasKepsekPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
