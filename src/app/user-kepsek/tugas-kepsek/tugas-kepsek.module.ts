import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TugasKepsekPageRoutingModule } from './tugas-kepsek-routing.module';

import { TugasKepsekPage } from './tugas-kepsek.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TugasKepsekPageRoutingModule
  ],
  declarations: [TugasKepsekPage]
})
export class TugasKepsekPageModule {}
