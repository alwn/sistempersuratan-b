import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TugasKepsekPage } from './tugas-kepsek.page';

const routes: Routes = [
  {
    path: '',
    component: TugasKepsekPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TugasKepsekPageRoutingModule {}
