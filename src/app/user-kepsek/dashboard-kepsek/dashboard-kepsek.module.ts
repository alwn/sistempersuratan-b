import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DashboardKepsekPageRoutingModule } from './dashboard-kepsek-routing.module';

import { DashboardKepsekPage } from './dashboard-kepsek.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DashboardKepsekPageRoutingModule
  ],
  declarations: [DashboardKepsekPage]
})
export class DashboardKepsekPageModule {}
