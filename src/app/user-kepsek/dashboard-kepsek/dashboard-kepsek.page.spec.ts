import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DashboardKepsekPage } from './dashboard-kepsek.page';

describe('DashboardKepsekPage', () => {
  let component: DashboardKepsekPage;
  let fixture: ComponentFixture<DashboardKepsekPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardKepsekPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DashboardKepsekPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
