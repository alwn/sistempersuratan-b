import { Component, OnInit } from '@angular/core';
import { AngularFirestore} from "angularfire2/firestore";

@Component({
  selector: 'app-dashboard-kepsek',
  templateUrl: './dashboard-kepsek.page.html',
  styleUrls: ['./dashboard-kepsek.page.scss'],
})
export class DashboardKepsekPage implements OnInit {

  constructor(
    public db: AngularFirestore
  ) { }

  ngOnInit() {
    this.ambilSuratMasuk();
    this.ambilSuratTerkirim();
    this.ambiltugas();
  }
  
  dataSuratMasuk: any=[];
  dataSuratTerkirim: any=[];
  datatugas: any=[];


  ambilSuratMasuk() {
    this.db.collection('terkirim-tu').valueChanges().subscribe(data => {
      this.dataSuratMasuk = data;
      console.log(this.dataSuratMasuk)
    })
  }

  ambilSuratTerkirim() {
    this.db.collection('terkirim-tu').valueChanges().subscribe(data => {
      this.dataSuratTerkirim = data;
      console.log(this.dataSuratTerkirim)
    })
  }

  ambiltugas() {
    this.db.collection('tugas').valueChanges().subscribe(data => {
      this.datatugas = data;
      console.log(this.datatugas)
    })
  }

}