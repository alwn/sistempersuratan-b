import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardKepsekPage } from './dashboard-kepsek.page';

const routes: Routes = [
  {
    path: '',
    component: DashboardKepsekPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardKepsekPageRoutingModule {}
