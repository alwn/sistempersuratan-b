import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfileKepsekPage } from './profile-kepsek.page';

const routes: Routes = [
  {
    path: '',
    component: ProfileKepsekPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileKepsekPageRoutingModule {}
