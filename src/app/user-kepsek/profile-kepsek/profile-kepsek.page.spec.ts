import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProfileKepsekPage } from './profile-kepsek.page';

describe('ProfileKepsekPage', () => {
  let component: ProfileKepsekPage;
  let fixture: ComponentFixture<ProfileKepsekPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileKepsekPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProfileKepsekPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
