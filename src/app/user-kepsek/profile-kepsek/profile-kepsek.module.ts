import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfileKepsekPageRoutingModule } from './profile-kepsek-routing.module';

import { ProfileKepsekPage } from './profile-kepsek.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfileKepsekPageRoutingModule
  ],
  declarations: [ProfileKepsekPage]
})
export class ProfileKepsekPageModule {}
