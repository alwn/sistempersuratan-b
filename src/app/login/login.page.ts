import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore  } from "@angular/fire/firestore";
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(
    public auth:AngularFireAuth,
    public db: AngularFirestore,
    public router: Router,
    private toastr: ToastController
  
  ) { }

  ngOnInit() {
  }

  email:any;
  password:any;
  masuk(){
    this.auth
    .auth
    .signInWithEmailAndPassword(this.email, this.password)
    .then(res => {
     this.cek_akses(res)
    })
    .catch(err => {
      this.toast('Email atau password yang anda masukkan salah', 'danger');
    });
  }

  data_user : any;
  cek_akses(res){
    console.log(res)
    var id_user = res.user.uid;
    this.db.collection("user").doc(id_user).valueChanges().subscribe(data => {
      console.log(data)
      this.data_user = data;
      if(this.data_user.role == "guru"){
        this.router.navigate(["/dashboard-guru"])
      }else if(this.data_user.role == "tata usaha"){
        this.router.navigate(["/dashboard-tu"])
      }else if(this.data_user.role == "kepala sekolah"){
        this.router.navigate(["/dashboard-kepsek"])
      }else{
        alert("gagal")
      }
    })
  }
  async toast(message, status)
  {
      const toast = await this.toastr.create({
          message:message,
          position: 'top',
          color: status,
          duration:2000
      });
      toast.present();
   }

}
