import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TambahsuratGuruPage } from './tambahsurat-guru.page';

const routes: Routes = [
  {
    path: '',
    component: TambahsuratGuruPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TambahsuratGuruPageRoutingModule {}
