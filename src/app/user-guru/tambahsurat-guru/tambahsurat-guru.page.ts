import { Component, OnInit } from '@angular/core';
import { AngularFirestore  } from "@angular/fire/firestore";

@Component({
  selector: 'app-tambahsurat-guru',
  templateUrl: './tambahsurat-guru.page.html',
  styleUrls: ['./tambahsurat-guru.page.scss'],
})
export class TambahsuratGuruPage implements OnInit {

  constructor(
    public db: AngularFirestore
  ) { }

  ngOnInit() {
  }

  data: any = {};
  tambah(){
    console.log(this.data)
    this.db.collection('disposisi').doc(this.data.no_surat).set(this.data).then(data => {
      alert('surat berhasil ditambahkan')
    })
  }
}
