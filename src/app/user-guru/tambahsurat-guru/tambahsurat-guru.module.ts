import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TambahsuratGuruPageRoutingModule } from './tambahsurat-guru-routing.module';

import { TambahsuratGuruPage } from './tambahsurat-guru.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TambahsuratGuruPageRoutingModule
  ],
  declarations: [TambahsuratGuruPage]
})
export class TambahsuratGuruPageModule {}
