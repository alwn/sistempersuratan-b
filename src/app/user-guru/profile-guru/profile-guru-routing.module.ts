import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfileGuruPage } from './profile-guru.page';

const routes: Routes = [
  {
    path: '',
    component: ProfileGuruPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileGuruPageRoutingModule {}
