import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProfileGuruPage } from './profile-guru.page';

describe('ProfileGuruPage', () => {
  let component: ProfileGuruPage;
  let fixture: ComponentFixture<ProfileGuruPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileGuruPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProfileGuruPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
