import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfileGuruPageRoutingModule } from './profile-guru-routing.module';

import { ProfileGuruPage } from './profile-guru.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfileGuruPageRoutingModule
  ],
  declarations: [ProfileGuruPage]
})
export class ProfileGuruPageModule {}
