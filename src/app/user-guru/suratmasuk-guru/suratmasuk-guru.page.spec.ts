import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SuratmasukGuruPage } from './suratmasuk-guru.page';

describe('SuratmasukGuruPage', () => {
  let component: SuratmasukGuruPage;
  let fixture: ComponentFixture<SuratmasukGuruPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuratmasukGuruPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SuratmasukGuruPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
