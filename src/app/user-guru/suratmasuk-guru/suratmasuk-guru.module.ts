import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SuratmasukGuruPageRoutingModule } from './suratmasuk-guru-routing.module';

import { SuratmasukGuruPage } from './suratmasuk-guru.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SuratmasukGuruPageRoutingModule
  ],
  declarations: [SuratmasukGuruPage]
})
export class SuratmasukGuruPageModule {}
