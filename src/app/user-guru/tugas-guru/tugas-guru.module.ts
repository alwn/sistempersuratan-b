import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TugasGuruPageRoutingModule } from './tugas-guru-routing.module';

import { TugasGuruPage } from './tugas-guru.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TugasGuruPageRoutingModule
  ],
  declarations: [TugasGuruPage]
})
export class TugasGuruPageModule {}
