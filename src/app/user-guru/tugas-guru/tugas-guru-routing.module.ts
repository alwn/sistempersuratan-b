import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TugasGuruPage } from './tugas-guru.page';

const routes: Routes = [
  {
    path: '',
    component: TugasGuruPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TugasGuruPageRoutingModule {}
