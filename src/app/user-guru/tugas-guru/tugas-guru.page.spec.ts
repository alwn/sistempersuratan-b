import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TugasGuruPage } from './tugas-guru.page';

describe('TugasGuruPage', () => {
  let component: TugasGuruPage;
  let fixture: ComponentFixture<TugasGuruPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TugasGuruPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TugasGuruPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
