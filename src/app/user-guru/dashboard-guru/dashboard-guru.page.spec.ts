import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DashboardGuruPage } from './dashboard-guru.page';

describe('DashboardGuruPage', () => {
  let component: DashboardGuruPage;
  let fixture: ComponentFixture<DashboardGuruPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardGuruPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DashboardGuruPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
