import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DashboardGuruPageRoutingModule } from './dashboard-guru-routing.module';

import { DashboardGuruPage } from './dashboard-guru.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DashboardGuruPageRoutingModule
  ],
  declarations: [DashboardGuruPage]
})
export class DashboardGuruPageModule {}
