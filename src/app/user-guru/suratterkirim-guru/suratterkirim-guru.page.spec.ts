import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SuratterkirimGuruPage } from './suratterkirim-guru.page';

describe('SuratterkirimGuruPage', () => {
  let component: SuratterkirimGuruPage;
  let fixture: ComponentFixture<SuratterkirimGuruPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuratterkirimGuruPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SuratterkirimGuruPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
