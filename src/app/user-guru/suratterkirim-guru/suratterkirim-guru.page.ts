import { Component, OnInit } from '@angular/core';
import { AngularFirestore  } from "@angular/fire/firestore";

@Component({
  selector: 'app-suratterkirim-guru',
  templateUrl: './suratterkirim-guru.page.html',
  styleUrls: ['./suratterkirim-guru.page.scss'],
})
export class SuratterkirimGuruPage implements OnInit {

  constructor(
    public db: AngularFirestore
  ) { }

  ngOnInit() {
    this.ambildata();

  }

  surat_terkirim: any;

  ambildata(){
    this.db.collection('disposisi').valueChanges().subscribe(data =>{
      this.surat_terkirim=data;
      console.log(this.surat_terkirim)
    } )
  }

}
