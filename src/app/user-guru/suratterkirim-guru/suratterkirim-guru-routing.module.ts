import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SuratterkirimGuruPage } from './suratterkirim-guru.page';

const routes: Routes = [
  {
    path: '',
    component: SuratterkirimGuruPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SuratterkirimGuruPageRoutingModule {}
