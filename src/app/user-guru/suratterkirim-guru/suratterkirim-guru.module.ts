import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SuratterkirimGuruPageRoutingModule } from './suratterkirim-guru-routing.module';

import { SuratterkirimGuruPage } from './suratterkirim-guru.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SuratterkirimGuruPageRoutingModule
  ],
  declarations: [SuratterkirimGuruPage]
})
export class SuratterkirimGuruPageModule {}
