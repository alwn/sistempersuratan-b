// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.



export const environment = {
  production: false,
  firebase: {apiKey: "AIzaSyCaNQe86rEx0L8GOCycbGjtHxqKmqYVzDA",
  authDomain: "sipanse-ca914.firebaseapp.com",
  projectId: "sipanse-ca914",
  storageBucket: "sipanse-ca914.appspot.com",
  messagingSenderId: "144014051557",
  appId: "1:144014051557:web:ecadf7f9b0c2975cb5ac0c"

  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
